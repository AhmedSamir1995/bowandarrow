﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {
    public Pool objectPool;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void addMeToThePool(GameObject go)
    {

        if(go.gameObject.GetComponent<Arrow>()&& !go.gameObject.GetComponent<Arrow>().hit)
        {
            GameManager.Instance.Streak = 0;
        }
        if (objectPool)
            objectPool.ReturnItem(go);
        else
            Destroy(go);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        addMeToThePool(collision.gameObject);
        
    }

}
