﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : MonoBehaviour {
    public int Speed;
    public Vector3 iceRelativePosition;
    public uint score;
    public LayerMask iceLayer;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        transform.position = transform.position + Vector3.up * Speed * Time.deltaTime;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Rom7");
        GameObject other=collision.gameObject;
        if (iceLayer != (iceLayer | (1 << other.layer)))
        {
            if (other.GetComponent<Destroyer>())
                other.GetComponent<Destroyer>().addMeToThePool(gameObject);
            return;
        }
        /*other.transform.SetParent(transform);
        other.GetComponent<Rigidbody2D>().simulated = false;
        other.transform.localPosition = iceRelativePosition;*/
        /*foreach (var i in GetComponents<SpriteRenderer>())
        {
            i.flipY = true;
        }*/

        if (GetComponent<Animator>())
        {
            GetComponent<Animator>().SetBool("Popped",true);
        }
        Speed = -6;
        collision.gameObject.GetComponent<Arrow>().hit = true;
        Destroy(gameObject.GetComponent<CircleCollider2D>());
        Destroy(gameObject, 3);
        GameManager.Instance.score += (int)score;

        GameManager.Instance.Streak += 1;
    }


}
