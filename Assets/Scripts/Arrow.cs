﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {
    Rigidbody2D rb2D;
    public bool hit;
	// Use this for initialization
	void Start () {
        rb2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        //transform.rotation = Quaternion.Euler(0, 0, (rb2D.velocity.x / rb2D.velocity.y) * 90);

        transform.right = rb2D.velocity;
	}
}
