﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public int score;
    public UnityEngine.UI.Text scoreText;
    public float time;
    public UnityEngine.UI.Text timeText;
    int streak;
    public UnityEngine.UI.Text streakText;
    public int streakThreshold;
    public int arrows;
    public UnityEngine.UI.Text arrowsText;
    public int Streak
    {
        get
        {
            return streak;
        }
        set
        {
            streak = value;
            if (streak >= streakThreshold)
                arrows+=2;
        }
    }
    public int Arrows
    {
        set
        {
            arrows = value;
        }
        get
        {
            return arrows;
        }
    }
    // Use this for initialization
    void Start () {
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        //Debug.Log(Instance);
        if(scoreText)
            scoreText.text = "Score= " + score.ToString();
        if (timeText)
            timeText.text = "Time= " + ((int)time).ToString();
        if (arrowsText)
        {
            arrowsText.text = "";// "↑= " + arrows.ToString();
            for(int i=0; i<arrows;i++)
            {
                arrowsText.text += "↑";
            }
        }
        if (scoreText)
            streakText.text = "Streak= " + streak.ToString();

    }

    static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                return new GameManager();
            }
            else
                return instance;
        }
    }
}
