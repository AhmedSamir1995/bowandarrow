﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    public GameObject poolObject;
    public int initialCount;
    public List<GameObject> objectsPool;
    public GameObject container;
    // Use this for initialization
    void Start()
    {
        GenerateNItems(initialCount);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ReturnItem(GameObject poolObject)
    {
        poolObject.SetActive(false);
        objectsPool.Add(poolObject);
    }
    public GameObject GetItem()
    {
        GameObject temp;
        if (objectsPool.Count <= 0)
        {
            GenerateNItems(2);
        }

        temp = objectsPool[0];
        objectsPool.RemoveAt(0);
        temp.SetActive(true);
        return temp;
    }

    void GenerateNItems(int N)
    {
        for (int i = 0; i < N; i++)
        {
            GameObject tempObject = GameObject.Instantiate(poolObject);
            tempObject.SetActive(false);
            if (container != null)
                tempObject.transform.SetParent(container.transform);
            else
                tempObject.transform.SetParent(transform);
            objectsPool.Add(tempObject);
        }
    }
}
