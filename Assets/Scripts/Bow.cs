﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : MonoBehaviour {
    public Pool ArrowPool;
    public float force;
    float ShootingForce;
    bool readyToShoot=false;
    LineRenderer lineRenderer;
    public GameObject forShowArrow;
    public float bowtomouseMag;
    public float maxBowMouseMagnitude;
	// Use this for initialization
	void Start () {
        lineRenderer = GetComponent<LineRenderer>();
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
		if(readyToShoot && Input.GetMouseButton(0))
        {

            forShowArrow.SetActive(true);
            //print("mouse");
            Vector3 BowToMouse =  transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            BowToMouse.z = 0;
            // print(BowToMouse.magnitude/2*force);
            BowToMouse = BowToMouse.normalized * Mathf.Min(BowToMouse.magnitude, maxBowMouseMagnitude);
            bowtomouseMag = BowToMouse.magnitude;
            forShowArrow.transform.localPosition = Vector3.right*(-1.05f -  BowToMouse.magnitude / 2);
            lineRenderer.SetPosition(1, Vector3.right * (-1.05f - BowToMouse.magnitude / 2));
            ShootingForce = BowToMouse.magnitude / 2 * force;
            Vector3 AngleZeroVector = Vector3.right;
            transform.rotation = Quaternion.Euler(0,0, Vector3.Angle(AngleZeroVector, BowToMouse)*Mathf.Sign(Vector3.Cross(AngleZeroVector,BowToMouse).z));
            //Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        if(readyToShoot&&Input.GetMouseButtonUp(0))
        {
            forShowArrow.SetActive(false);
            readyToShoot = false;
            Shoot();

            forShowArrow.transform.localPosition = Vector3.right * -1.05f;
            lineRenderer.SetPosition(1, Vector3.right * -1.05f);
        }
        if(Input.GetMouseButtonDown(0))
        {
            //readyToShoot = true;
        }
	}
    private void OnMouseUp()
    {
       /* GameObject arrow= ArrowPool.GetItem();
        arrow.transform.SetParent(transform);
        arrow.transform.localScale = Vector3.one;
        arrow.transform.localPosition = new Vector3(-2.64f, 0, 0);
        arrow.transform.localRotation = Quaternion.identity;
        //arrow.GetComponent<Rigidbody2D>().AddTorque(-9.8f);
        arrow.GetComponent<Rigidbody2D>().AddForce(transform.right * force,ForceMode2D.Impulse);*/
    }

    private void OnMouseDown()
    {
        if(!UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            readyToShoot = true;
    }

    void Shoot()
    {
        if (GameManager.Instance.arrows > 0)
        {
            GameObject arrow = ArrowPool.GetItem();
            //arrow.transform.SetParent(transform);
            arrow.transform.localScale = Vector3.one;
            arrow.transform.position = forShowArrow.transform.position;
            arrow.transform.localRotation = Quaternion.identity;
            //arrow.GetComponent<Rigidbody2D>().AddTorque(-9.8f);
            arrow.GetComponent<Arrow>().hit = false;
            arrow.GetComponent<Rigidbody2D>().centerOfMass = new Vector2(-20.54f, 0);
            arrow.GetComponent<Rigidbody2D>().AddForce(transform.right * ShootingForce, ForceMode2D.Impulse);
            GameManager.Instance.arrows--;
        }
    }
}
