﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonGenerator : MonoBehaviour {
    BoxCollider2D boxCollider2D;
    public Pool BalloonPool;
    public float minDeltaGeneration;
    public float maxDeltaGeneration;
    float nextGeneration;
    public List<Color> colorPalette;
	// Use this for initialization
	void Start () {
        boxCollider2D = GetComponent<BoxCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time>nextGeneration)
        {
            GenerateBaloon();
        }
	}

    public void GenerateBaloon()
    {
        Vector3 newBalloonPosition = transform.position + 
            (Vector3)boxCollider2D.offset +
        new Vector3(Random.Range(-boxCollider2D.size.x / 2, boxCollider2D.size.x / 2),
        Random.Range(-boxCollider2D.size.y / 2, boxCollider2D.size.y / 2),
        0);
        nextGeneration = Time.time + Random.Range(minDeltaGeneration, maxDeltaGeneration);
        newBalloonPosition.x = Mathf.RoundToInt(newBalloonPosition.x);
        newBalloonPosition.y = Mathf.RoundToInt(newBalloonPosition.y);
        newBalloonPosition.z = Mathf.RoundToInt(newBalloonPosition.z);
        GameObject balloon = BalloonPool.GetItem();
        balloon.transform.position = newBalloonPosition;
        if(colorPalette.Count>0)
        balloon.GetComponent<SpriteRenderer>().color = colorPalette[Random.Range(0, colorPalette.Count)];

    }
}
